DESCRIPTION
-----------
Enables users to choose a responsive image style when embedding images into text using CKEditor and the Media Entity Browser.

CONFIGURATION
-------------
 * Enable the text format filter 'Display responsive images' and select the images styles/responsive styles
   that you want to be available to the user.
 * Make sure that the 'Limit allowed HTML tags' text filter is enabled but you will need to add the following to the 'allowed tags'
 list: <drupal-entity data-* data-entity-type data-entity-uuid data-entity-embed-display data-entity-embed-display-settings data-align
 data-caption data-embed-button alt title class> <picture data-* alt title class> <source srcset media type>
 * Make sure that the 'Restrict images to this site' text filter is disabled.
 * The user will then be prompted to select a responsive image style when inserting an image via CKEditor
